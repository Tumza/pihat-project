# PiHAT Project

PiHAT design project for EEE3088F:

The HAT is used to drive a motor using a driver IC, 
and power the motor up using a PSU for the motor. 
It also includes circuitry needed to monitor motor position.
